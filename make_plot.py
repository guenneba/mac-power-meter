import argparse

import pandas as pd
import numpy as np
from scipy.signal import savgol_filter
import math
import matplotlib.pyplot as plt
from datetime import datetime
import matplotlib.dates as mdates
import json
import numbers
import re
from el4000 import el4000
from pandas.plotting import register_matplotlib_converters
import regression_validation as rv
import seaborn as sns
register_matplotlib_converters()

if __name__ == "__main__":

    ap = argparse.ArgumentParser()

    ap.add_argument("-s", "--sensors", required=False, help="sensors csv log file")
    ap.add_argument("-c", "--config", required=False, help="config.json", default="pm_config.json")
    ap.add_argument("-a", "--activity", required=False, help="my_activties.json")
    ap.add_argument("-w", "--el4", required=False, nargs='+', help="Voltcraft 4000 log file(s) in either .BIN or .csv formats.")
    ap.add_argument("-t", "--timestep", required=False, help="temporal resolution", default=60, type=int)
    ap.add_argument("-d", "--timeshift", required=False, help="shift voltcraft data by the given number of seconds", default=0, type=int)

    args = vars(ap.parse_args())

    max_val = 0
    fig, ax = plt.subplots()
    fig.set_size_inches(20, 6)

    # ---------------------------------------
    # load and process power_meter's log file
    # ---------------------------------------
    if args['sensors']:

        with open(args['config']) as f:
            pmconfig = json.load(f)

        csv_delim = ','
        if 'csv_delim' in pmconfig:
            csv_delim = pmconfig['csv_delim']
        data = pd.read_csv(args['sensors'], delimiter=csv_delim) # , error_bad_lines=False 
        data = data.rename(columns=lambda x: x.strip())

        timeheader = pmconfig['time']['header']
        
        # remove any rows containing empty cells
        data.dropna(axis=0, how='any', thresh=None, subset=None, inplace=True)

        if 'Core' in data and 'CPU' in data:
            # this is a turbostat datafile, let's keep summary rows only:
            data = data[data['Core']=='-']

        if 'current A' in data and 'voltage' in data:
            I = data['current A'].values
            U = data['voltage'].values
            P = U*I
            data['batt-out'] = np.clip(-P, a_min=0, a_max=None)
            data['batt-in']  = np.clip( P, a_min=0, a_max=None)
        elif 'charge' in data or 'charge Ah' in data:
            # add instant battery power
            # data['charge'] = data['charge'].values*1e-3*12
            if 'charge' in data:
                charge = data['charge'].values
                voltage = 1
            else:
                charge = data['charge Ah'].values
                voltage = data['voltage'].values
                data['charge'] = charge*voltage
            # # smooth data before computing derivatives
            # dt = data['time'][1]-data['time'][0]
            # ws = max(3,int(2*60/dt))
            # if ws%2==0:
            #     ws = ws+1
            # charge = savgol_filter(charge, ws, 2)
            dc  = np.diff(charge)
            dc = np.append(dc,[dc[-1]])
            dt = np.diff(data['time'])
            dt = np.append(dt,[dt[-1]])
            data['batt-in'] = dc * voltage / dt * 3600

        if 'format' in pmconfig['time']:
            base_date = datetime.fromtimestamp(0)
            if 'filename-date-format' in pmconfig['time']:
                # extract date from filename
                base_date_str = re.search(pmconfig['time']['filename-date-regex'], args['sensors'])
                if base_date_str:
                    base_date = datetime.strptime(base_date_str[0], pmconfig['time']['filename-date-format'])
            # convert to timestamp
            data[timeheader] = data[timeheader].apply(lambda x: datetime.timestamp(datetime.combine(base_date.date(), datetime.strptime(x, pmconfig['time']['format']).time() )))

        base_time = data[timeheader].values[0]
        # step_seconds = data[timeheader].values[2]-data[timeheader].values[1];

        end=data[timeheader].values.size
        start = 0
        avg_size=args['timestep']
        n = end-start
        end = int(start+math.floor(n/avg_size)*avg_size)

        def downsample(serie, step_size, insert_zeros):
            vals = serie.apply(pd.to_numeric, errors='coerce').values
            ts = data[timeheader].values
            res = []
            i = 0
            curr_end = ts[0]+step_size
            curr_sum = 0.0
            curr_n = 0.0
            while i<ts.size:
                while i<ts.size and ts[i]<curr_end:
                    if vals[i]<refval*1e5:
                        curr_sum += vals[i]
                        curr_n += 1
                    i += 1
                if curr_n>0:
                    res.append(curr_sum/float(curr_n))
                elif insert_zeros:
                    res.append(0)
                else:
                    res.append(res[-1]+step_size)
                curr_end += step_size
                curr_sum = 0.
                curr_n = 0.
            return np.array(res)

        times = downsample(data[timeheader],avg_size,False)
        times = [datetime.fromtimestamp(d) for d in times]

        for item in pmconfig['series']:
            if 'key' in item:
                if item['key'] in data:
                    values = downsample(data[item['key']], avg_size, True)
                    max_val = max(max_val,np.max(values))
                    ax.plot(times, values, label=item['label'])
                    if 'rename' in item:
                        print("rename...")
                        data = data.rename(columns={item['key']:item['rename']})
                else:
                    print("WARNING skip missing column \'{}\'".format(item['key']))

    # ---------------------------------------
    # load and process voltcraft log file
    # ---------------------------------------

    if args['el4']:
        filename0 = args['el4'][0]
        if filename0.lower().endswith('.bin'):
            data4000 = el4000.load_el4(args['el4'])
        else:
            data4000 = pd.read_csv(filename0)
        
        # rewrite timestamps:
        delta_t = args['timeshift']
        data4000['timestamp'] = data4000['timestamp'].apply(lambda x: datetime.timestamp(datetime.strptime(x, "%Y-%m-%d %H:%M"))+delta_t)
        # add watts:
        data4000 = data4000.assign(W=data4000['voltage'].values*data4000['current'].values*data4000['power_factor'].values)

        max_val = max(max_val,np.max(data4000['W'].values))

        ax.plot([datetime.fromtimestamp(d) for d in data4000['timestamp'].values], data4000['W'].values, label='P_active (W)')
    
    ax.set(xlabel='time', ylabel='Watts')
    #plt.xticks(np.arange(0, proc_watt.size, 5))
    plt.legend()
    ax.grid()


    # ---------------------------------------
    # compute summaries
    # ---------------------------------------

    start=0
    end=0
    start_el4=0
    end_el4=0
    if args['sensors']:
        ts = downsample(data[timeheader],60,False)
        end=len(ts)
        ts = downsample(data[timeheader],60,False)
        start=0
        end=len(ts)

    if args['el4']:
        end_el4 = data4000['timestamp'].size

    if args['sensors'] and args['el4']:
        # find common start
        time_start = int(math.floor(ts[0]/60)*60)
        common_start = max(time_start, data4000['timestamp'].values[0])
        start = int((common_start-time_start)/60)
        start_el4 = int((common_start-data4000['timestamp'].values[0])/60)
        # find common end
        common_len = min(len(ts)-start,data4000['timestamp'].size-start_el4)
        end=start+common_len
        end_el4=start_el4+common_len

    
    print("\nSummaries:")
    print(  "----------")
    if args['sensors']:
        for item in pmconfig['summaries']:
            if 'key' in item:
                values = downsample(data[item['key']], 60, True)
                #values = data[item['key']].apply(lambda x: x if x<1e11 else 0).values
                total = np.sum( 0.5*(values[start:end-1]+values[start+1:end]) * (ts[start+1:end]-ts[start:end-1]) )/3600
                print(' * {}: {:.3f} kWh'.format(item['label'], total/1000))

    if args['el4']:
        total = np.sum(data4000['W'].values[start_el4:end_el4])/60
        print(' * EL400 total: {:.3f} kWh'.format(total/1000))


    
    # ---------------------------------------
    # regression / prediction
    # ---------------------------------------
    if args['sensors'] and 'RAPL Pkg Tot' in data:
        if args['el4']:
            tot_values = data4000['W'].values[start_el4:end_el4]
        elif ('TOTAL' in data.columns) and ('RAPL Pkg Tot' in data.columns):
            tot_values = downsample(data['TOTAL'], avg_size, True)[start:end]

        RAPL_values = downsample(data['RAPL Pkg Tot'], avg_size, True)[start:end]
        if 'RAPL DRAM' in data:
            RAPL_values += downsample(data['RAPL DRAM'], avg_size, True)[start:end]
        A = np.vstack((RAPL_values>0,RAPL_values))
        gpu_usage = np.array([])
        coeff_labels = "P_predict = {:.2f} + {:.2f}*(RAPL Cpu+DRAM)"
        titles = ['Constant','RAPL Cpu+DRAM']
        
        if 'gpu device 0' in data:
            gpu_usage  = downsample(data['gpu device 0'], avg_size, True)[start:end]
        elif 'gpu W 0' in data:
            gpu_usage  = downsample(data['gpu 0'], avg_size, True)[start:end]
        if gpu_usage.size>0:
            A = np.vstack((A, gpu_usage))
            coeff_labels = coeff_labels + " + {:.2f}*GPU"
            titles.append("GPU")
            if np.min(gpu_usage)==0 and np.max(data['RAPL Pkg Gfx'].values)>0:
                # we add an activation cost only if we have two GPUs
                A = np.vstack((A, gpu_usage>0))
                coeff_labels = coeff_labels + " + {:.2f}*(GPU>0?1:0)"
                titles.append("GPU>0?1:0")
        
        def add_linear_factor(cname,label):
            global A
            global coeff_labels
            if cname in data:
                vals  = downsample(data[cname], avg_size, True)[start:end]
                if np.max(vals)>0:
                    A = np.vstack((A,vals))
                    coeff_labels = coeff_labels + " + {:.2f}*" + label
                    titles.append(label)

        add_linear_factor('batt-in',    'battIn')
        add_linear_factor('brightness', 'brightness')
        

        A = A.T
        # x = np.linalg.lstsq(A,tot_values,rcond=None)[0]
        x = np.linalg.lstsq(A,tot_values)[0]
        # print("\nLinear regression coeffs: {}".format(x))
        
        pred = np.dot(A,x.T)
        ax.plot(times[start:end], pred, label="predict")
        total = np.sum(pred)/60
        print(' * predict total: {:.3f} kWh'.format(total/1000))
        print("")
        print(coeff_labels.format(*x))

        #-----------------------------
        # Robustness of the parameters
        #-----------------------------

        # 1. Shuffle and split, fixed test_size
        list_coeffs,explained_variance = rv.cross_validate(A,tot_values,n_split=1000,test_size=0.5,random_state=1)
        n_coeffs=list_coeffs.shape[1]
        m = int(np.ceil(np.sqrt(n_coeffs)))
        plt.figure()
        for coeff in range(n_coeffs):
            plt.subplot(m,m,coeff+1)
            p=sns.kdeplot(list_coeffs[:,coeff], shade=True)
            plt.title(titles[coeff])

        # x, y = p.get_lines()[0].get_data()

        # 2. Shuffle and split, various test_size
        list_test_size= np.arange(0.1,0.9,0.05)
        label_test_size=['{:.1f}'.format(i) for i in list_test_size]
        all_lists=[[],[],[],[]]
        all_variance=[]
        for test_size in list_test_size:
            list_coeffs, explained_variance = rv.cross_validate(A, tot_values, n_split=1000, test_size=test_size, random_state=1)
            for coeff in range(n_coeffs):
                all_lists[coeff].append(list_coeffs[:,coeff])
            all_variance.append(explained_variance)

        plt.figure()
        for coeff in range(n_coeffs):
            plt.subplot(2,2,coeff+1)
            p=sns.boxplot(data=all_lists[coeff])
            p.set_xticklabels(label_test_size)
            plt.xlabel('Test size')
            plt.title(titles[coeff])

        plt.figure()
        p=sns.boxplot(data=all_variance)
        p.set_xticklabels(label_test_size)
        plt.xlabel('Test size')
        plt.ylabel('Explained variance')


    # ---------------------------------------
    # load and process activity log file
    # ---------------------------------------

    if args['activity']:
        with open(args['activity']) as f:
          activity_data = json.load(f)

        dates = []
        names = []

        for item in activity_data:
            item_ts = item['time']
            if isinstance(item_ts, numbers.Number):
                if item_ts>1e10:
                    item_ts /= 1000
                dates.append(datetime.fromtimestamp(item_ts))
            else:
                dates.append(datetime.strptime(item_ts,"%d/%m/%Y %H:%M:%S"))
            names.append(item['activity'])

        #dates = [datetime.fromtimestamp(d) for d in dates]

        # Plot timeline
        levels = np.tile([-5, -1, 3, -3, 1, 5],
                         int(np.ceil(len(dates)/6)))[:len(dates)]*0.5+max_val+5

        markerline, stemline, baseline = ax.stem(dates, levels,
                                                 linefmt="C0--", basefmt="", use_line_collection=True)

        baseline.set_visible(False)
        markerline.set_visible(False)

        # annotate lines
        vert = np.array(['top', 'bottom'])[(levels > 0).astype(int)]
        for d, l, r, va in zip(dates, levels, names, vert):
            ax.annotate(r, xy=(d, l), xytext=(2, np.sign(l)*3-10),
                        textcoords="offset points", va=va, ha="left")

    plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    plt.ylim(bottom=0)
    ax.margins(y=0.1)

    ax.get_xaxis().set_major_formatter(mdates.DateFormatter("%m-%d %H:%M"))

    plt.show()
    fig.savefig("plot.png")
    
    