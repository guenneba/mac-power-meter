

cmake_minimum_required(VERSION 3.8...3.17)

if(${CMAKE_VERSION} VERSION_LESS 3.12)
    cmake_policy(VERSION ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
endif()

project(power_meter VERSION 0.1
                  DESCRIPTION "A tool to monitor and log several power consumption related sensors"
                  LANGUAGES CXX)

set(power_meter_src src/power_meter.cpp	src/rapl.cpp src/smcsampler.cpp src/usage_info.cpp)
if(APPLE)
  set(power_meter_src ${power_meter_src} src/smc.cpp src/usage_info.mm )
else()
  set(power_meter_src ${power_meter_src} src/usage_info.cpp )
endif()

#include_directories(${PROJECT_SOURCE_DIR})

add_executable(power_meter ${power_meter_src})
target_compile_features(power_meter PUBLIC cxx_std_14)

if(${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
  set(POWERMETER_USE_UPOWER OFF CACHE BOOL "Enable the use of upower for battery monitoring")
  if(POWERMETER_USE_UPOWER)
    find_package(PkgConfig REQUIRED)
    pkg_search_module(GLIB REQUIRED glib-2.0)
    target_include_directories(power_meter PRIVATE ${GLIB_INCLUDE_DIRS})

    target_link_libraries(power_meter PUBLIC -lupower-glib)
    target_compile_definitions(power_meter PUBLIC -DUSE_UPOWER)
  endif()
endif()


if(APPLE)
  add_executable(smccat src/smcstat.cpp src/smc.cpp)
  target_compile_features(smccat PUBLIC cxx_std_14)

  set(frameworks "-framework IOKit" "-framework CoreFoundation" "-framework Foundation")
  target_link_libraries(power_meter PUBLIC ${frameworks})
  target_link_libraries(smccat PUBLIC ${frameworks})
endif()
