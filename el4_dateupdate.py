import argparse
import pandas as pd
import numpy as np
from datetime import datetime

if __name__ == "__main__":

    ap = argparse.ArgumentParser()

    ap.add_argument("-w", "--el4", required=True, help="Voltcraft 4000 csv log file")
    ap.add_argument("-d", "--datetime", required=True, help="start date time YYYY/mm/dd HH:MM")

    args = vars(ap.parse_args())


    data4000 = pd.read_csv(args['el4'])
    base_ts = datetime.timestamp(datetime.strptime(data4000['timestamp'][0], "%Y-%m-%d %H:%M"))
    new_start = datetime.timestamp(datetime.strptime(args['datetime'], "%Y/%m/%d %H:%M"))
    # rewrite timestamps:
    data4000['timestamp'] = data4000['timestamp'].apply(lambda x:
        datetime.strftime(datetime.fromtimestamp(datetime.timestamp(datetime.strptime(x, "%Y-%m-%d %H:%M"))-base_ts+new_start), "%Y-%m-%d %H:%M"))

    print(data4000)
    data4000.to_csv("tmp.csv", index=False)
    