

#pragma once

#if __APPLE__
#include <IOKit/IOKitLib.h>
#endif
#include "sampler.h"
#include <string>

class SMCSampler : public AccumulatingSampler
{
	std::vector<std::string> m_sensors;
  #if __APPLE__
	io_connect_t m_connection;
  #endif
public:
	
	SMCSampler(const std::vector<std::string>& sensors);

	void accumulate_values();

	~SMCSampler();

	void stop();
};
