
#pragma once

#include <vector>

class AccumulatingSampler
{
protected:

	std::vector<double> m_values;

	void resize(int nb_items) {
		m_values.clear();
		m_values.resize(nb_items, 0.0);
	}

public:

	AccumulatingSampler(int nb_items)
		: m_values(nb_items, 0.0)
	{}

	const std::vector<double>& values() const { return m_values; }
	
	virtual void accumulate_values() = 0;

	std::vector<double> averages(int nb_samples)
	{
		double n = nb_samples;
		for(auto& v : m_values)
			v /= n;
		std::vector<double> tmp(m_values.size(),0);
		tmp.swap(m_values);
		return tmp;
	}

	virtual ~AccumulatingSampler() {}
	virtual void stop() {}
};
