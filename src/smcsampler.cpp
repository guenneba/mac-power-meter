
#include "smcsampler.h"

#if __APPLE__

#include <iostream>
#include "smc.h"
#include "parse_smc_value.h"

SMCSampler::SMCSampler(const std::vector<std::string>& sensors)
  : AccumulatingSampler(sensors.size()), m_sensors(sensors)
{
  if (kIOReturnSuccess != SMCOpen("AppleSMC", &m_connection)) {
    std::cerr << "ERROR Unable to open SMC connection\n";
    return;
  }
}
void SMCSampler::accumulate_values()
{
  for(int k=0; k<m_sensors.size(); ++k)
  {
    const char* sensor = m_sensors[k].c_str();
    SMCVal_t val;
    if (kIOReturnSuccess != SMCReadKey(m_connection, sensor, &val)) {
      std::cerr << "WARNING Unable to read key <" <<  sensor << ">\n";
      continue;
    }
    double actual_val = parse_smc_value(&val);
    m_values[k] += actual_val;
  }
}

void SMCSampler::stop()
{
  SMCClose(m_connection);
}

#else

SMCSampler::SMCSampler(const std::vector<std::string>& sensors)
  : AccumulatingSampler(sensors.size()), m_sensors(sensors)
{
}
void SMCSampler::accumulate_values()
{
}

void SMCSampler::stop()
{
}

#endif



SMCSampler::~SMCSampler()
{
  stop();
}