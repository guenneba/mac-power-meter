
#pragma once
#include <vector>
#include "sampler.h"

struct gpu_info {
  gpu_info(double c, double v, double d, double p=0)
    : core_use(c), video_use(v), device_use(d), power_W(p)
  {}
  double core_use;
  double video_use;
  double device_use;
  double power_W;
};

class GpuSampler : public AccumulatingSampler
{
  int m_nb_gpus;
public:
	
	GpuSampler();

	void accumulate_values();

  std::vector<gpu_info> averages(int nb_samples);

	~GpuSampler();

	void stop();
};

std::vector<gpu_info> get_gpu_info();
std::string gpu_header0(int i);
std::string gpu_header1(int i);
std::string gpu_header2(int i);
std::string gpu_header_csv(int i);
void gpu_print_row(const gpu_info& gpu);
std::string gpu_csv_row(const gpu_info& data);

struct battery_info {
    float charge_Ah = 0;
    float capacity_Ah = 0;
    float current_A = 0;
    float voltage = 0;
    bool on_battery = false;
};

battery_info get_battery_info();

float get_brightness_info();