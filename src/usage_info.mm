// clang++ gpu_mac.mm -framework IOKit -framework CoreFoundation -framework Foundation
// https://stackoverflow.com/questions/10110658/programmatically-get-gpu-percent-usage-in-os-x
#include <CoreFoundation/CoreFoundation.h>
#include <Cocoa/Cocoa.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/graphics/IOAccelClientConnect.h>
#import <IOKit/graphics/IOGraphicsLib.h>

#include <IOKit/pwr_mgt/IOPM.h>
#include <IOKit/pwr_mgt/IOPMlib.h>
// #include <IOKit/ps/IOPSKeys.h>
#include <iostream>
#include "usage_info.h"

// $ ioreg -r -d 1 -w 0 -c "IOAccelerator"
// +-o nvAccelerator  <class nvAccelerator, id 0x1000003fc, registered, matched, active, busy 0 (716 ms), retain 4631>
//     {
//       "IOClass" = "nvAccelerator"
//       "MetalStatisticsName" = "GeForceStatistics"
//       "IOVARendererSubID" = <03000000>
//       "IODVDBundleName" = "GeForceVADriver"
//       "NVDA,Enable-A2R10G10B10Format" = <01000000>
//       "NVDA,mm-version" = 4
//       "IOReportLegendPublic" = Yes
//       "IOGLBundleName" = "GeForceGLDriver"
//       "IOProviderClass" = "IOService"
//       "AGCInfo" = {"fBusyCount"=0,"poweredOffByAGC"=Yes,"fSubmissionsSinceLastCheck"=0,"fLastSubmissionPID"=550}
//       "MetalPluginName" = "GeForceMTLDriver"
//       "IOProbeScore" = 100
//       "SurfaceList" = ()
//       "AAPL,OpenCLdisabled" = Yes
//       "IOSourceVersion" = "12.0.23"
//       "IOPropertyMatch" = ({"NVArch"="GK100","nubType"="NVDAgl","unmatched"="yes","NVDAType"="Official"})
//       "PerformanceStatisticsAccum" = {"orphanedReusableVidMemoryHitRate"=0,"texturePageOutBytes"=0,"swapCompleteVideoWaitTime"=0,"orphanedReusableSysMemoryHitRate"=8,"dataBufferCount"=0,"oolTexturePageInBytes"=0,"textureCount"=2834,"iosurfaceTextureCreationCount"=0,"surfaceReadLockIdleWaitTime"=0,"freeDataBufferWaitTime"=0,"orphanedReusableSysMemoryBytes"=0,"ioSurfaceReadOutBytes"=0,"agprefTextureCreationCount"=0,"orphanedReusableSysMemoryCount"=0,"finishAll2DWaitTime"=0,"orphanedNonReusableSysMemoryCount"=0,"surfaceTextureCreationCount"=0,"ioSurfacePageOutBytes"=0,"surfaceBufferReadOutBytes"=0,"textureReadOutBytes"=0,"gartMapInBytesPerSample"=0,"dataBytesPerSample"=0,"swapBytesPerSample"=0,"surfaceCount"=94,"clientGLWaitTime"=0,"surfaceCopyInWaitTime"=0,"surfaceCopyOutWaitTime"=0,"gartMapOutBytesPerSample"=0,"ioSurfacePageInBytes"=0,"gartCacheBytes"=134217728,"vramUsedBytes"=4096,"volatileSurfaceCount"=0,"vramEvictionWaitTime"=0,"oolTextureCreationBytes"=0,"freeToAllocGPUAddressWaitTime"=0,"oolTextureCreationCount"=0,"orphanedReusableVidMemoryCount"=0,"orphanedReusableVidMemoryBytes"=0,"context2DCount"=4,"inUseSysMemoryBytes"=368316416,"bufferSwapCount"=0,"orphanedNonReusableVidMemoryBytes"=0,"contextGLCount"=26,"finish2DWaitTime"=0,"gartSizeBytes"=5726625792,"recoveryCount"=0,"stdTexturePageInBytes"=0,"agprefTextureCreationBytes"=0,"gartUsedBytes"=15003648,"surfaceWriteLockIdleWaitTime"=0,"finishGLWaitTime"=0,"surfaceBufferPageInBytes"=0,"iosurfaceTextureCreationBytes"=0,"finishCLWaitTime"=0,"finishVideoWaitTime"=0,"contextCLCount"=17,"freeSurfaceBackingWaitTime"=0,"swapCompleteGLWaitTime"=0,"orphanedNonReusableSysMemoryBytes"=0,"inUseVidMemoryBytes"=368316416,"vramLargestFreeBytes"=2147483648,"agpTextureCreationBytes"=0,"stdTextureCreationBytes"=0,"agpTextureCreationCount"=0,"stdTextureCreationCount"=0,"sysmemUsedBytes"=0,"surfaceSetShapeIdleWaitTime"=0,"gartFreeBytes"=5711622144,"hardwareSubmitWaitTime"=0,"bufferFlipCount"=0,"textureVolunteerUnloadBytes"=0,"swapComplete2DWaitTime"=0,"surfaceBufferTextureCreationCount"=0,"surfaceBufferPageOutBytes"=0,"hardwareWaitTime"=0,"Device Utilization %"=0,"freeSurfaceSwapBufferWaitTime"=0,"contextVideoCount"=0,"orphanedNonReusableVidMemoryCount"=0}
//       "MetalPluginClassName" = "NVMTLDevice"
//       "IOAccelRevision" = 2
//       "IOCFPlugInTypes" = {"ACCF0000-0000-0000-0000-000a2789904e"="IOAccelerator2D.plugin"}
//       "IOAccelDisplayPipeCapabilities" = {"DisplayPipeSupported"=Yes,"TransactionsSupported"=Yes}
//       "InternalStatisticsAccm" = {}
//       "IOMatchCategory" = "IOAccelerator"
//       "CFBundleIdentifier" = "com.apple.GeForce"
//       "IOVABundleName" = "GeForceVADriver"
//       "InternalStatistics" = {}
//       "PerformanceStatistics" = ...
std::vector<gpu_info> get_gpu_info()
{
    std::vector<gpu_info> res;

    // Get dictionary of all the PCI Devicces
    CFMutableDictionaryRef matchDict = IOServiceMatching(kIOAcceleratorClassName);

    // Create an iterator
    io_iterator_t iterator;

    if (IOServiceGetMatchingServices(kIOMasterPortDefault,matchDict,
                                     &iterator) == kIOReturnSuccess)
    {
        // Iterator for devices found
        io_registry_entry_t regEntry;

        while ((regEntry = IOIteratorNext(iterator))) {
            // Put this services object into a dictionary object.
            CFMutableDictionaryRef serviceDictionary;
            if (IORegistryEntryCreateCFProperties(regEntry,
                                                  &serviceDictionary,
                                                  kCFAllocatorDefault,
                                                  kNilOptions) != kIOReturnSuccess)
            {
                // Service dictionary creation failed.
                IOObjectRelease(regEntry);
                continue;
            }

            // to see what's available:
            // $ ioreg -r -d 1 -w 0 -c "IOAccelerator"
            CFMutableDictionaryRef perf_properties = (CFMutableDictionaryRef) CFDictionaryGetValue( serviceDictionary, CFSTR("PerformanceStatistics") );
            if (perf_properties) {

                static ssize_t gpuCoreUse=-10000000;
                static ssize_t gpuVideoUse=-1;
                static ssize_t deviceUse=-1;

                const void* gpuCoreUtilization = CFDictionaryGetValue(perf_properties, CFSTR("GPU Core Utilization"));
                const void* gpuVideoUtilization = CFDictionaryGetValue(perf_properties, CFSTR("GPU Video Engine Utilization"));
                const void* deviceUtilization = CFDictionaryGetValue(perf_properties, CFSTR("Device Utilization %"));
                if (gpuCoreUtilization)
                    CFNumberGetValue( (CFNumberRef) gpuCoreUtilization, kCFNumberSInt64Type, &gpuCoreUse);
                // else
                //     printf("GPU Core Utilization not found\n");
                if (gpuVideoUtilization)
                    CFNumberGetValue( (CFNumberRef) gpuVideoUtilization, kCFNumberSInt64Type, &gpuVideoUse);
                // else
                //     printf("GPU Video Engine Utilization not found\n");
                if (deviceUtilization)
                    CFNumberGetValue( (CFNumberRef) deviceUtilization, kCFNumberSInt64Type, &deviceUse);
                // else
                //     printf("Device Utilization not found\n");
                
                res.emplace_back(gpuCoreUse/(double)10000000,(double)gpuVideoUse,(double)deviceUse);
            }

            CFRelease(serviceDictionary);
            IOObjectRelease(regEntry);
        }
        IOObjectRelease(iterator);
    }

   return res;
}

// https://gist.github.com/andermoran/2195a4ecd4f212cbd2c36afba1f58cdb
// https://stackoverflow.com/questions/3239749/programmatically-change-mac-display-brightness
float get_brightness_info()
{
    io_iterator_t iterator;
    kern_return_t result = IOServiceGetMatchingServices(kIOMasterPortDefault, IOServiceMatching("IODisplayConnect"), &iterator);
                                                    
    // If we were successful
    if (result == kIOReturnSuccess)
    {
        io_object_t service;
        while ((service = IOIteratorNext(iterator))) {
            // Print the current brightness of the screen
            float current_brightness;
            IODisplayGetFloatParameter(service, kNilOptions, CFSTR(kIODisplayBrightnessKey), &current_brightness);

            // Let the object go
            IOObjectRelease(service);
            return current_brightness;
        }
    }

   return 0;
}

// https://gist.github.com/atr000/295390
// https://developer.apple.com/documentation/iokit/1557138-iopmcopybatteryinfo?language=objc

battery_info get_battery_info()
{
    static mach_port_t master = 0;
    static io_connect_t pmcon = 0;
    CFArrayRef batteries = NULL;
    int batteryCount = 0;
    
    battery_info res;

    if (master == 0)
    {
        IOMasterPort(bootstrap_port, &master);
        pmcon = IOPMFindPowerManagement(master);
    }
    if (IOPMCopyBatteryInfo(master, &batteries) == kIOReturnSuccess)
    {
        CFNumberRef flagsnum;
        long flags;
        if (batteries)
            batteryCount = CFArrayGetCount(batteries);
        if (batteryCount > 0)
        {
            CFDictionaryRef battery0 = (CFDictionaryRef)CFArrayGetValueAtIndex(batteries, 0);
            
            const void *val = 0;
            val = CFDictionaryGetValue(battery0, CFSTR(kIOBatteryFlagsKey));

            CFNumberGetValue((CFNumberRef)val, kCFNumberLongType, &flags);

            int charge_mAh;
            val = CFDictionaryGetValue(battery0, CFSTR(kIOBatteryCurrentChargeKey));
            CFNumberGetValue((CFNumberRef)val, kCFNumberSInt32Type, &charge_mAh);

            int capacity_mAh;
            val = CFDictionaryGetValue(battery0, CFSTR(kIOBatteryCapacityKey));
            CFNumberGetValue((CFNumberRef)val, kCFNumberSInt32Type, &capacity_mAh);

            int ampere_mA;
            val = CFDictionaryGetValue(battery0, CFSTR(kIOBatteryAmperageKey));
            CFNumberGetValue((CFNumberRef)val, kCFNumberSInt32Type, &ampere_mA);

            int voltage_mV;
            val = CFDictionaryGetValue(battery0, CFSTR(kIOBatteryVoltageKey));
            CFNumberGetValue((CFNumberRef)val, kCFNumberSInt32Type, &voltage_mV);

            res.charge_Ah = charge_mAh*1e-3;
            res.capacity_Ah = capacity_mAh*1e-3;
            res.current_A = ampere_mA*1e-3;
            res.voltage = voltage_mV*1e-3;

            res.on_battery = !(flags & kIOBatteryChargerConnect);
        } else {
            std::cerr << "no battery\n";
        }
    } else {
        std::cerr << "IOPMCopyBatteryInfo failed\n";
    }
    return res;
}
