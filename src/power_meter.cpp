
#include "smcsampler.h"
#include "usage_info.h"
#include "rapl.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <chrono>
#include <cstring>
#include <functional>
#include <ctime>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>

auto stamp() {
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

static void Controller_alrm_handler(int aSigNum, siginfo_t* aInfo, void* aContext);

class Controller
{
public:
    using Callback = std::function<void(int,int)>;

protected:

    static Controller* ms_instance;
    
    std::vector<AccumulatingSampler*> m_samplers;

    int m_sampling_step_ms;
    int m_callback_step_sec;
    int m_nb_samples;
    
    
    Callback m_callback;
    std::chrono::time_point<std::chrono::steady_clock> m_start;

public:

    static Controller& instance() { return *ms_instance; }

    Controller(int sampling_step_ms)
        : m_sampling_step_ms(sampling_step_ms), m_callback_step_sec(1000*sampling_step_ms), m_nb_samples(0), m_callback(0)
    {
        if(ms_instance!=0) {
            std::cerr << "ERROR, you cannot create multiple Controller instances\n";
            return;
        }
        ms_instance = this;

        struct sigaction sa;
      memset(&sa, 0, sizeof(sa));
      sa.sa_flags = SA_RESTART | SA_SIGINFO;
      sigemptyset(&sa.sa_mask);
      sa.sa_sigaction = Controller_alrm_handler;
      if (sigaction(SIGALRM, &sa, NULL) < 0) {
        std::cerr << "ERROR sigaction(SIGALRM) failed\n";
        return;
      }

      // Set up the timer.
      struct itimerval timer;
      timer.it_interval.tv_sec = m_sampling_step_ms / 1000;
      timer.it_interval.tv_usec = (m_sampling_step_ms % 1000) * 1000;
      timer.it_value = timer.it_interval;
      if (setitimer(ITIMER_REAL, &timer, NULL) < 0) {
        std::cerr << "ERROR setitimer() failed\n";
        return;
      }

        m_start = std::chrono::steady_clock::now();
    }

    void add(AccumulatingSampler* pSampler)
    {
        m_samplers.push_back(pSampler);
    }

    void set_callback(int callback_step_sec, const Callback& cb) {
        m_callback_step_sec = callback_step_sec;
        m_callback = cb;
    }

    ~Controller() {
        for(auto pSampler : m_samplers)
            pSampler->stop();
    }

    void tick()
    {
        for(auto pSampler : m_samplers)
            pSampler->accumulate_values();
        
        ++m_nb_samples;

        auto current = std::chrono::steady_clock::now();
        auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(current-m_start).count();
        if(m_callback && (elapsed_ms>=1000*m_callback_step_sec || m_nb_samples==m_callback_step_sec*1000/m_sampling_step_ms)) {
            m_callback(elapsed_ms, m_nb_samples);
            m_start = current;
            m_nb_samples = 0;
        }
    }

    void stop()
    {
        for(auto pSampler : m_samplers)
            pSampler->stop();
        exit(0);
    }
};

Controller* Controller::ms_instance = 0;

void Controller_alrm_handler(int aSigNum, siginfo_t* aInfo, void* aContext)
{
    Controller::instance().tick();
}

void int_handler(int aSigNum, siginfo_t* aInfo, void* aContext)
{
    Controller::instance().stop();
}

int main(int argc, char** argv) {

    int step_seconds = 10;

    if(argc==2) {
        step_seconds = std::atoi(argv[1]);
    }

    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    std::ostringstream oss;
    oss << std::put_time(&tm, "%d-%m-%Y_%H-%M-%S");
    std::string filename = "power_meter_log_" + oss.str() + ".csv";
    std::ofstream csv(filename);

    using str_pair = std::pair<std::string,std::string>;
    std::vector<str_pair> sensors = {
        // str_pair{"IM0R", "   DRAM "}, // PCPD, PM0R
        str_pair{"PCPC", "Pkg Core"},
        str_pair{"PCPG", "Pkg Gfx "},
        str_pair{"PCPT", "Pkg Tot "},
        str_pair{"PDTR", "     AC "}, // includes battery charging, 0 when on-battery
        str_pair{"PSTR", "  TOTAL "}
    };
    double sums_Wh = 0;
    RAPL rapl;

    {
        struct sigaction sa;
        memset(&sa, 0, sizeof(sa));
        sa.sa_flags = SA_RESTART | SA_SIGINFO;
        sigemptyset(&sa.sa_mask);
            sa.sa_sigaction = int_handler;
        if (sigaction(SIGINT, &sa, NULL) < 0) {
            std::cerr << "ERROR sigaction(SIGINT) failed\n";
        }
    }
    int nb_gpus = get_gpu_info().size();
    (void)get_battery_info();
    (void)get_brightness_info();

    csv << "\"time\"";
    for(int k=0; k<sensors.size(); ++k) {
        if(k!=0)
            printf("|");
        printf("%s",sensors[k].second.c_str());

        csv << ",\"" << sensors[k].second.c_str() << '\"';
    }
    printf("|  TOTAL ");
    printf("||Pkg Core|Pkg Gfx |Pkg Tot | DRAM ");
    printf("|| disp ");
    printf("||         battery        ");

    for(int i=0; i<nb_gpus; ++i) {
        printf("||");
        std::cout << gpu_header0(i);
    }
    printf("\n");

    for(int k=0; k<sensors.size(); ++k) {
        if(k!=0) printf("|");
        printf("    (W) ");
    }
    printf("|    Wh  ");
    printf("||          RAPL Watts             ");
    printf("||   %%  "); // brightness
    printf("|| on|   Ah |   A  |   V  ");

    for(int i=0; i<nb_gpus; ++i) {
        printf("||");
        std::cout << gpu_header1(i);
    }
    printf("\n");

    csv << ",\"TOTAL Wh\",\"RAPL Pkg Core\",\"RAPL Pkg Gfx\",\"RAPL Pkg Tot\",\"RAPL DRAM\",\"brightness\",\"on battery\",\"charge Ah\",\"current A\",\"voltage\"";

    for(int i=0; i<nb_gpus; ++i) {
        csv << "," << gpu_header_csv(i);
    }
    csv << "\n";

    for(int k=0; k<sensors.size(); ++k) {
        if(k!=0)
            printf("+");
        printf("--------");
    }
    printf("+--------"); // total Wh
    printf("++--------+--------+--------+------"); // RAPL
    printf("++------"); // brightness
    printf("++---+------+------+------"); // battery

    for(int i=0; i<nb_gpus; ++i) {
        printf("++");
        std::cout << gpu_header2(i);
    }
    printf("\n");

    Controller ctrl(1000);
    SMCSampler smc({"PCPC","PCPG","PCPT","PDTR","PSTR"});
    GpuSampler gpu;
    ctrl.add(&smc);
    ctrl.add(&gpu);
    ctrl.set_callback(step_seconds,[&smc,&gpu,&sums_Wh,&sensors,&csv,&rapl](int elapsed_ms, int nb_samples){
        
        double step_seconds = elapsed_ms/1000.;

        csv << '\"' << stamp() << '\"';

        auto smc_avg = smc.averages(nb_samples);
        for(int k=0; k<sensors.size(); ++k) {
            double instant_watts = smc_avg[k];

            if(k==sensors.size()-1)
                sums_Wh += instant_watts*step_seconds / 3600;

            if(k!=0)
                printf("|");
            printf("%8.2f", instant_watts);

            csv << ",\"" << instant_watts << '\"';
        }
        printf("|%8.2f", sums_Wh);
        csv << ",\"" << sums_Wh << '\"';

        // RAPL stuff
        {
            // A special value that represents an estimate from an unsupported RAPL domain.
            const double kUnsupported_j = -1.0;
            double pkg_J=0, cores_J=0, gpu_J=0, ram_J=0;
            rapl.EnergyEstimates(pkg_J, cores_J, gpu_J, ram_J);

            auto asWatts = [step_seconds,kUnsupported_j](double aValue_J) {
                if (aValue_J == kUnsupported_j) {
                    return -1.;
                } else {
                    // Power = Energy / Time, where power is measured in Watts, Energy is measured
                    // in Joules, and Time is measured in seconds.
                    return aValue_J/double(step_seconds);
                }
            };
            // Pkg Tot |Pkg Core|Pkg Gfx | DRAM
            printf("||%8.2f|%8.2f|%8.2f|%6.2f",asWatts(cores_J),asWatts(gpu_J),asWatts(pkg_J),asWatts(ram_J));
            csv << ",\"" << asWatts(cores_J) << "\",\"" << asWatts(gpu_J) << "\",\"" << asWatts(pkg_J) << "\",\"" << asWatts(ram_J) << '\"';
        }

        double brightness = get_brightness_info();
        printf("|| %5.3f", brightness);
        csv << ",\"" << brightness << '\"';

        auto battery = get_battery_info();
        printf("|| %d |%6.3f|%6.3f|%6.3f", battery.on_battery, double(battery.charge_Ah), double(battery.current_A), double(battery.voltage));
        csv << ",\"" << int(battery.on_battery) << "\",\"" << battery.charge_Ah << "\",\"" << battery.current_A << "\",\"" << battery.voltage << '\"';

        //const auto& gpus = gpu;
        auto gpu_avg = gpu.averages(nb_samples);
        for(auto& item : gpu_avg) {
            printf("||");
            gpu_print_row(item);
            csv << "," << gpu_csv_row(item);
        }
        printf("\n");
        csv << '\n';
        csv.flush();
        
        auto start = std::chrono::steady_clock::now();

    });

    while(true) pause();

    csv.close();

    printf("\n");
}
