
#include <sstream>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include "usage_info.h"

#ifdef __linux__
#define LINUX_BACKEND
#endif


#ifdef LINUX_BACKEND

#include <sys/stat.h>
#include <unistd.h>

inline bool file_exists(const std::string& name)
{
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

long read_long(const std::string& filename) {
  long val = -1;
  std::ifstream f(filename);
  if(f) {
    f >> val;
  }
  return val;
}

std::string read_string(const std::string& filename) {
  std::string val;
  std::ifstream f(filename);
  if(f) {
    f >> val;
  }
  return val;
}
#endif


GpuSampler::GpuSampler()
  : AccumulatingSampler(0)
{
  m_nb_gpus = get_gpu_info().size();
  this->resize(m_nb_gpus*4);
}

GpuSampler::~GpuSampler() {}

void GpuSampler::accumulate_values()
{
  auto gpus = get_gpu_info();
  for(int k=0; k<m_nb_gpus; ++k)
  {
    auto& gpu = gpus[k];
    m_values[4*k+0] += gpu.core_use;
    m_values[4*k+1] += gpu.video_use;
    m_values[4*k+2] += gpu.device_use;
    m_values[4*k+3] += gpu.power_W;
  }
}

std::vector<gpu_info> GpuSampler::averages(int nb_samples) {
  auto raw = static_cast<AccumulatingSampler*>(this)->averages(nb_samples);

  std::vector<gpu_info> res;
  for(int i=0; i<m_nb_gpus; ++i) {
    res.emplace_back(raw[4*i+0],raw[4*i+1],raw[4*i+2],raw[4*i+3]);
  }
  return res;
}

void GpuSampler::stop() {}

#if defined(LINUX_BACKEND)

#include <regex>
#include <cstdio>
#include <array>
#include <stdexcept>

std::string gpu_header0(int i) { return "    GPU " + std::to_string(i)
                                               + "   "; }
std::string gpu_header1(int i) { return "   % | Watt "; }
std::string gpu_header2(int i) { return "-----+------"; }
std::string gpu_header_csv(int i) {
  std::string si = std::to_string(i);
  return "\"gpu " + si + "\",\"gpu W " + si + "\"";
}
void gpu_print_row(const gpu_info& gpu) {
  printf("%5.1f|%6.1f", gpu.device_use, gpu.power_W);
}
std::string gpu_csv_row(const gpu_info& gpu) {
  std::stringstream ss;
  ss << "\"" << gpu.device_use << "\",\"" << gpu.power_W << '\"';
  return ss.str();
}

std::string exec(const char* cmd)
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

std::vector<gpu_info> get_gpu_info()
{
  // try using nvidia-smi
  static bool first = true;
  static bool have_nvidia_smi = true;

  const char* cmd = "nvidia-smi -q  -d UTILIZATION,POWER";

  std::string output;
  std::vector<gpu_info> res;

  if(first) {
    if(system("nvidia-smi -L")!=0) {
      have_nvidia_smi = false;
      std::cerr << "WARNING failed to call nvidia-smi, GPU monitoring disabled\n";
    }
    first = false;
  }

  if(have_nvidia_smi) {
    try {
      output = exec(cmd);
    } catch (...) {        
      return {};
    }

    // parse output
    double percent, power;

    std::smatch m;
    {
      // "        Gpu                         : 0 %"
      std::regex e("\\s*Gpu\\s*:\\s*([0-9]*\\.*[0-9]*) %");
      if(std::regex_search (output,m,e))
        percent = std::atof(std::string(m[1]).c_str());
    }
    {
      // "        Power Draw                  : 15.51 W"
      std::regex e("\\s*Power Draw\\s*:\\s*([0-9]*\\.*[0-9]*) W");
      if(std::regex_search (output,m,e))
        power = std::atof(std::string(m[1]).c_str());
    }

    res.emplace_back(-1,-1,percent,power);
  }

  return res;
}

#elif defined(__APPLE__)

std::string gpu_header0(int i) { return "        GPU " + std::to_string(i)
                                                   + "          "; }
std::string gpu_header1(int i) { return " core | video | device "; }
std::string gpu_header2(int i) { return "------+-------+--------"; }
std::string gpu_header_csv(int i) {
  std::string si = std::to_string(i);
  return "\"gpu core " + si + "\",\"gpu video " + si + "\",\"gpu device " + si + "\"";
}
void gpu_print_row(const gpu_info& gpu) {
  printf("%6.2f|%7.2f|%8.2f", gpu.core_use, gpu.video_use, gpu.device_use);
}
std::string gpu_csv_row(const gpu_info& gpu) {
  std::stringstream ss;
  ss << "\"" << gpu.core_use << "\",\"" << gpu.video_use << "\",\"" << gpu.device_use << '\"';
  return ss.str();
}

#else

std::vector<gpu_info> get_gpu_info()
{
  static bool first = true;
  if(first)
    std::cerr << "WARNING get_gpu_info not implemented yet for your system\n";
  first = false;
  return {};
}

#endif





#ifdef LINUX_BACKEND

#include <string>
#include <fstream>
#include <regex>
#include <vector>
#include <dirent.h>

float get_brightness_info()
{
  static bool first = true;
  static std::string backlight_path;
  static bool has_brightness_files = false;
  if(first)
  {
    // search backlight's brightness files:
    const std::string base_path{"/sys/class/backlight/"};
    struct dirent *entry = nullptr;
    DIR *dp = nullptr;
    dp = opendir(base_path.c_str());
    if (dp != nullptr) {
      while ((!has_brightness_files) && (entry = readdir(dp))) {
        std::string filename = base_path + entry->d_name;
        if (/*(entry->d_type==DT_DIR) &&*/ file_exists(filename+"/brightness") && file_exists(filename+"/max_brightness")) {
          has_brightness_files = true;
          backlight_path = filename;
        }
      }
      closedir(dp);
      if(!has_brightness_files) {
        std::cerr << "WARNING no brightness info in " << base_path << "\n";  
      }
    } else {
      std::cerr << "WARNING " << base_path << " not found\n";
    }

    
    first = false;
  }

  if(has_brightness_files) {
    float brightness = read_long(backlight_path+"/brightness");
    float max_brightness = read_long(backlight_path+"/max_brightness");
    return brightness/max_brightness;
  }

  return 0;
}

#elif !defined(__APPLE__)

float get_brightness_info()
{
  static bool first = true;
  if(first)
    std::cerr << "WARNING get_brightness_info not implemented yet for your system\n";
  first = false;
  return 0;
}
#endif





#ifdef LINUX_BACKEND


#ifdef USE_UPOWER

// upower-based backend

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <glib.h>
#include <glib/gi18n-lib.h>
#include <locale.h>

#include <libupower-glib/upower.h>

#include <regex>

// upower backend

// exemple:
  // native-path:          /sys/devices/LNXSYSTM:00/device:00/PNP0C0A:00/power_supply/BAT0
  // vendor:               NOTEBOOK
  // model:                BAT
  // serial:               0001
  // power supply:         yes
  // updated:              Thu Feb  9 18:42:15 2012 (1 seconds ago)
  // has history:          yes
  // has statistics:       yes
  // battery
  //   present:             yes
  //   rechargeable:        yes
  //   state:               charging
  //   energy:              22.3998 Wh
  //   energy-empty:        0 Wh
  //   energy-full:         52.6473 Wh
  //   energy-full-design:  62.16 Wh
  //   energy-rate:         31.6905 W
  //   voltage:             12.191 V
  //   time to full:        57.3 minutes
  //   percentage:          42.5469%
  //   capacity:            84.6964%
  //   technology:          lithium-ion

battery_info get_battery_info()
{
  battery_info res;
  static bool first = true;

#if !defined(GLIB_VERSION_2_36)
	g_type_init ();
#endif
	setlocale(LC_ALL, "");

  int error;
	UpClient *client = up_client_new_full(nullptr, &error);
	if (client == nullptr) {
    if(first)
		  std::cerr << "WARNING Cannot connect to upowerd: " << error->message << "\n";
		g_error_free (error);
	}
  else
  {

    // search for battery path name
    std::string battery_path; // = "/org/freedesktop/UPower/devices/battery_BAT0";
    {
      GPtrArray *devices = up_client_get_devices2(client);
      for(int i=0; i<devices->len; ++i)
      {
        UpDevice* device = (UpDevice*)g_ptr_array_index(devices, i);
        std::string object_path = up_device_get_object_path(device);
        if(first)
          std::cout << "UPower: " << object_path << "\n";
        if(object_path.find("BAT") != std::string::npos) {
          if(first)
            std::cout << "UPower, found battery at: " << object_path << "\n";
          battery_path = object_path;
        }
      }
      g_ptr_array_unref(devices);

      device = up_client_get_display_device(client);
      g_print ("%s\n", up_device_get_object_path (device));
      g_object_unref(device);
    }

    if(!battery_path.empty())
    {
      UpDevice *device = up_device_new();
      bool ret = up_device_set_object_path_sync(device, battery_path.c_str(), nullptr, &error);
      if (!ret) {
        if(first)
          std::cerr << "WARNING: failed to set path: " << error->message << "\n";
        g_error_free (error);
      } else {
        gchar* text = up_device_to_text(device);
        if(first)
          std::cerr << "BAT : " << text << "\n";
        
        // parse stuff...

        std::string s(msg);

        std::smatch m;
        {
          std::regex e("energy:\\s*([0-9]*\\.*[0-9]*) Wh");
          if(std::regex_search (s,m,e))
            res.charge_Wh = std::atof(std::string(m[1]).c_str());
        }

        {
          std::regex e("energy-full:\\s*([0-9]*\\.*[0-9]*) Wh");
          if(std::regex_search (s,m,e))
            res.capacity_Wh = std::atof(std::string(m[1]).c_str());
        }

        {
          std::regex e("power supply:\\s*no");
          res.on_battery = std::regex_search (s,e);
        }

        g_free (text);
      }
      g_object_unref(device);
    }
    
    g_object_unref(client);
  }
  first = false;

  return res;
}

#else

// backend based on /sys/class/power_supply
// https://www.kernel.org/doc/Documentation/power/power_supply_class.txt

#include <string>
#include <fstream>
#include <regex>
#include <vector>


battery_info get_battery_info()
{
  battery_info res;
  static bool first = true;
  static std::string path;
  static bool has_battery_file = false;
  const bool use_uevent = false;
  if(first)
  {
    std::vector<std::string> candidates{"/sys/class/power_supply/BAT0","/sys/class/power_supply/BAT1","/sys/class/power_supply/battery"};
    for(const auto& p : candidates) {
      const std::string f = p + "/uevent";
      if(file_exists(f)) {
        path = p;
        has_battery_file = true;
        break;
      }
    }
    if(!has_battery_file) {
      std::cerr << "WARNING no battery info found in /sys/class/power_supply/*/uevent\n";
    }
    first = false;
  }

  if(has_battery_file) {

    double charge_microAh, capacity_microAh, voltage_microV, current_microA;
    if(use_uevent) {
      std::ifstream f(path+"/uevent");
      std::string msg((std::istreambuf_iterator<char>(f)),
                      std::istreambuf_iterator<char>());

      auto get_long = [&msg] (const std::string& key) {
        std::smatch m;
        std::regex e(key+"=([0-9]*)");
        if(std::regex_search (msg,m,e))
          return std::atol(std::string(m[1]).c_str());
        else
          return 0l;
      };
      
      res.on_battery = !std::regex_search(msg, std::regex("POWER_SUPPLY_STATUS=Charging"));

      charge_microAh = get_long("POWER_SUPPLY_CHARGE_NOW");
      capacity_microAh = get_long("POWER_SUPPLY_CHARGE_FULL");
      voltage_microV = get_long("POWER_SUPPLY_VOLTAGE_NOW");
    } else {
      charge_microAh = read_long(path+"/charge_now");
      capacity_microAh = read_long(path+"/charge_full");
      voltage_microV = read_long(path+"/voltage_now");
      current_microA = read_long(path+"/current_now");

      res.on_battery = read_string(path+"/status") != "Charging";
    }

    res.voltage = voltage_microV*1e-6;
    res.current_A = current_microA*1e-6;
    res.charge_Ah = charge_microAh*1e-6;
    res.capacity_Ah = capacity_microAh*1e-6;
  }

  return res;
}

#endif

#elif !defined(__APPLE__)

battery_info get_battery_info()
{
  static bool first = true;
  if(first)
    std::cerr << "WARNING get_battery_info not implemented yet for your system\n";
  first = false;
  return {};
}

#endif
