
* SMC
  * http://cargocultelectronics.com/2014/06/mac-power-consumption/
  * https://github.com/kzlekk/HWSensors/tree/master/HWMonitor/Profiles
  * https://web.archive.org/web/20160525201657/http://jbot-42.github.io:80/Articles/smc.html
  * https://web.archive.org/web/20150107044816/http://www.parhelia.ch/blog/statics/k3_keys.html
  * https://github.com/xythobuz/JSystemInfoKit/blob/master/SystemInfoKit/JSKSMC.m 
  * https://web.archive.org/web/20160525005847/http://jbot-42.github.io/systeminfokit.html
  * https://github.com/gch1p/smctool
  * https://superuser.com/questions/553197/interpreting-sensor-names
  * https://github.com/beltex/SMCKit
  * https://github.com/VoluntaryLabs/SystemInfoKit
  * https://github.com/FergusInLondon/SMCWrapper

  
* IOKit power management
  * https://developer.apple.com/documentation/iokit/iopmlib_h?language=objc

* RAPL
  * https://searchfox.org/mozilla-central/source/tools/power/rapl.cpp 


* Battery
  * https://stackoverflow.com/questions/1432792/how-to-get-the-battery-life-of-mac-os-x-macbooks-programatically
  * https://developer.apple.com/documentation/kernel/iopmpowersource?language=objc


# Linux

* Battery
  * https://stackoverflow.com/questions/30776050/reading-battery-status-on-linux-ubuntu-using-qt
    * /sys/class/power_supply
    * /sys/class/power_supply/AC/online
    * /sys/class/power_supply/ADP0/online
    * /sys/class/power_supply/BAT0/capacity
    * upower
  * https://askubuntu.com/questions/69556/how-do-i-check-the-batterys-status-via-the-terminal
  * https://unix.stackexchange.com/questions/227918/system-event-on-ac-adapter-insert-or-battery-unplugged
  * 

* Brightness
  * https://stackoverflow.com/questions/5811279/possible-to-change-screen-brightness-with-c
    * /sys/class/backlight/*/brightness
    * 
  * utility:
    * `xbacklight -set 100`
    * for external monitor ddccontrol:
      * https://askubuntu.com/questions/894465/changing-the-screen-brightness-of-the-external-screen

# Windows

* Battery
  * https://stackoverflow.com/questions/7785096/read-laptop-battery-status-in-float-double
    * SYSTEM_POWER_STATUS spsPwr; if( GetSystemPowerStatus(&spsPwr) ) { ... }
  
