import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.model_selection import ShuffleSplit
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import LeavePOut
from sklearn.metrics import explained_variance_score


def cross_validate(X,y,n_split=100,test_size=0.4,random_state=0):
    # # 1: CV
    #X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)
    # # 2: CV
    #lpo = LeavePOut(2)
    #print(lpo.get_n_splits(X))
    # #3: Shuffle split
    rs = ShuffleSplit(n_splits=n_split, test_size=test_size, random_state=random_state)
    # Create model
    linreg = LinearRegression(fit_intercept=False)
    list_coeffs=[]
    list_explained_variance=[]
    #for train_index, test_index in lpo.split(X):
    for train_index, test_index in rs.split(X):
        #print("TRAIN:", train_index, "TEST:", test_index)
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        linreg.fit(X_train, y_train)
        # Calculate our y hat (how our model performs against the test data held off)
        y_hat_test = linreg.predict(X_test)
        list_coeffs.append(linreg.coef_)
        list_explained_variance.append(explained_variance_score(y_test,y_hat_test))
    list_coeffs = np.array(list_coeffs)

    return list_coeffs, list_explained_variance