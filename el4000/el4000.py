
# This code has been adapted from https://github.com/Lekensteyn/el4000
# Copyright (C) 2014 Peter Wu <peter@lekensteyn.nl>
# Copyright (C) 2020 Gael Guennebaud <gael.guennebaud@inria.fr>

import os, sys
import datetime
import pandas as pd
from el4000.defs import info, data_hdr, data, setup, SETUP_MAGIC, STARTCODE

def process_file(filename, entries, dt, data_only):
    with open(filename, 'rb') as f:
        size = os.fstat(f.fileno()).st_size
        if size == info.size():
            # Info files
            t = info.parse_from_file(f)
            # Initialize time from info file
            dt[0] = datetime.datetime(2000 + t.init_date_year,
                    t.init_date_month, t.init_date_day,
                    t.init_time_hour, t.init_time_minute)
        else:
            # Data files.
            buf = f.read(len(SETUP_MAGIC))

            eof = 4 * b'\xff'
            while True:
                if len(buf) < len(eof):
                    buf += f.read(len(eof) - len(buf))
                if not buf:
                    break
                elif buf == eof[0:len(buf)]:
                    # End of file code (or short read at the end)
                    #sys.stdout.flush()
                    #_logger.info('EOF detected, skipping')
                    #continue
                    break

                if buf[0:len(STARTCODE)] == STARTCODE:
                    # Not data, but header before data
                    buf += f.read(data_hdr.size() - len(buf))
                    t = data_hdr.unpack(buf)
                    # New time reference!
                    dt[0] = datetime.datetime(2000 + t.record_year,
                            t.record_month, t.record_day,
                            t.record_hour, t.record_minute)
                    #printer.print_data_header(t)
                else:
                    buf += f.read(data.size() - len(buf))
                    t = data.unpack(buf)
                    # For time reference
                    date_str = dt[0].strftime('%Y-%m-%d %H:%M')
                    entries['timestamp'].append(date_str)
                    # print(*t)
                    # print(data.names)
                    for i,att in enumerate(data.names):
                        entries[att].append(t[i])
                    # Assume that this is called for every minute
                    dt[0] += datetime.timedelta(minutes=1)

                # Clear buffer since it is processed
                buf = b''


def load_el4(filenames):
    
    entries = {
            'timestamp':[],
            'voltage':[],
            'current':[],
            'power_factor': []
        }

    # Unknown date and time, initialize with something low.
    dt = [datetime.datetime(1970, 1, 1)]

    for filename in filenames:
        process_file(filename, entries, dt, True)
    
    return pd.DataFrame(entries, columns = ['timestamp', 'voltage', 'current', 'power_factor'])
